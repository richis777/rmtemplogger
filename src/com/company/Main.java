package com.company;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

    private final static Logger LOGGER = Logger.getLogger(Main.class.getName());
    private static final Properties prop = new Properties();
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
    private static final String SEPARATOR = ",";
    private static final long DEFAULT_INTERVAL = 60;
    public static final String TEMP_LOG_TXT = "tempLog.txt";
    public static final String SETTINGS_PROPERTIES = "settings.properties";

    public static void main(String[] args) {
        try {
            loadPops();
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Can't load settings.properties", e);
            return;
        }

        while (true) {
            String temp = getTemp();
            if (temp == null) continue;
            writeToFile(temp);

            try {
                Thread.sleep(getInterval() * 1000);
            } catch (InterruptedException e) {
                LOGGER.log(Level.SEVERE, "Thread.sleep", e);
            }
        }
    }

    private static long getInterval() {
        String interval = prop.getProperty("logging_interval_in_sec");
        try {
            return Long.parseLong(interval);
        } catch (NumberFormatException e) {
            LOGGER.log(Level.SEVERE, "Can't read logging_interval_in_sec param!", e);
            return DEFAULT_INTERVAL;
        }

    }

    private static void writeToFile(String temp) {
        try {
            File file = new File(TEMP_LOG_TXT);
            if (!file.exists()) file.createNewFile();

            String dataLine = dateFormat.format(new Date()) + SEPARATOR + temp + "\n";
            Files.write(Paths.get(TEMP_LOG_TXT), dataLine.getBytes(), StandardOpenOption.APPEND);
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Can't write to file!", e);
        }
    }

    private static void loadPops() throws IOException {
        InputStream in = new FileInputStream(SETTINGS_PROPERTIES);
        prop.load(in);
        in.close();

    }

    public static String getTemp() {

        try {
            URL oracle = new URL("http://"
                    + prop.getProperty("ip")
                    + "/" + prop.getProperty("password")
                    + "/t");

            URLConnection yc = oracle.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
            String temp = in.readLine();
            in.close();
            return temp;
        } catch (IOException e) {
            e.printStackTrace();
            LOGGER.log(Level.SEVERE, "getTemp()", e);
            return null;
        }
    }
}
